<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BlocksController;
use App\Http\Controllers\SeatsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/stadium', [BlocksController::class, 'index']);
Route::post('/stadium', [BlocksController::class, 'show']);
Route::get('/', function() {
	return view('welcome');
});

Route::get('/seats', [SeatsController::class, 'index']);
Route::post('/seating', [SeatsController::class, 'show']);
