<?php

namespace App\Http\Controllers;

use App\Models\Tiers;
use Illuminate\Http\Request;

class TiersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tiers  $tiers
     * @return \Illuminate\Http\Response
     */
    public function show(Tiers $tiers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tiers  $tiers
     * @return \Illuminate\Http\Response
     */
    public function edit(Tiers $tiers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tiers  $tiers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tiers $tiers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tiers  $tiers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tiers $tiers)
    {
        //
    }
}
