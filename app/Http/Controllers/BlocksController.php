<?php

namespace App\Http\Controllers;

use App\Models\Blocks;
use Illuminate\Http\Request;
use App\Football\Football;

use Log;

class BlocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $ballBallBall = new Football;

        $blocks = Blocks::select('id')->get();

        return view('stadium', ['blocks' => $blocks]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blocks  $blocks
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $blockData = Blocks::where('id', $request->block)->with('tier')->with('tier.stand')->firstOrFail();

        return view('block', ['blockData' => $blockData]);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blocks  $blocks
     * @return \Illuminate\Http\Response
     */
    public function edit(Blocks $blocks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blocks  $blocks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blocks $blocks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blocks  $blocks
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blocks $blocks)
    {
        //
    }
}
