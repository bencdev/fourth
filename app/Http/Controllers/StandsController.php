<?php

namespace App\Http\Controllers;

use App\Models\Stands;
use Illuminate\Http\Request;

class StandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
	dd('yop');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stands  $stands
     * @return \Illuminate\Http\Response
     */
    public function show(Stands $stands)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stands  $stands
     * @return \Illuminate\Http\Response
     */
    public function edit(Stands $stands)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stands  $stands
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stands $stands)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stands  $stands
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stands $stands)
    {
        //
    }
}
