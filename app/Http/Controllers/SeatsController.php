<?php

namespace App\Http\Controllers;

use App\Models\Seats;
use Illuminate\Http\Request;

use Log;

class SeatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Creating an array rather than pulling from a DB, might be easier to SQL this....  

        $suitableRows = array();
        $rowCount = 1;

        // Requested seats - 1 with counter starting at zero.

        $seatingChart = array();
        $seatingChart[0] = array(0,0,1,0,1,0,1,0,1,0,1,0,1);
        $seatingChart[1] = array(1,0,1,1,1,1,0,0,1,0,1,0,1);
        $seatingChart[2] = array(1,1,0,1,0,1,0,0,1,1,1,0,0);
        $seatingChart[3] = array(0,1,0,1,1,0,0,1,0,1,0,0,1);
        $seatingChart[4] = array(0,0,0,1,0,0,1,0,1,1,0,1,0);
        $seatingChart[5] = array(1,0,0,0,0,1,1,0,1,0,0,0,1);


        return view('seating', ['seatingChart' => $seatingChart]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Seats  $seats
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $suitableRows = array();
        $rowCount = 1;

        // Requested seats - 1 with counter starting at zero.
        $requestedSeats = $request->seats - 1;

        $seatingChart = array();
        $seatingChart[0] = array(0,0,1,0,1,0,1,0,1,0,1,0,1);
        $seatingChart[1] = array(1,0,1,1,1,1,0,0,1,0,1,0,1);
        $seatingChart[2] = array(1,1,0,1,0,1,0,0,1,1,1,0,0);
        $seatingChart[3] = array(0,1,0,1,1,0,0,1,0,1,0,0,1);
        $seatingChart[4] = array(0,0,0,1,0,0,1,0,1,1,0,1,0);
        $seatingChart[5] = array(1,0,0,0,0,1,1,0,1,0,0,0,1);


        foreach($seatingChart as $seatingRow)
        {

            $previous = null;
            $consecutive = 0;

            foreach($seatingRow as $seat)
            {

                if(isset($previous) && $previous == 1 && $seat == 1)
                {
                    $consecutive++;

                    log::error($consecutive);


                    if($consecutive == $requestedSeats)
                    {

                        log::error($consecutive);
                        array_push($suitableRows, $rowCount);
                    }
                }else
                {
                    $consecutive = 0;
                }

                $previous = $seat;

            }

            $rowCount++;

        }

        $suitableRows = array_unique($suitableRows);

        return view('seating', ['seatingChart' => $seatingChart, 'suitableRows' => $suitableRows, 'requestedSeats' => $request->seats]);        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Seats  $seats
     * @return \Illuminate\Http\Response
     */
    public function edit(Seats $seats)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Seats  $seats
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seats $seats)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Seats  $seats
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seats $seats)
    {
        //
    }
}
