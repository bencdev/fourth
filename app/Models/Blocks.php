<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blocks extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    public function tier()
    {
    	return $this->belongsTo(Tiers::class);
    }
}
