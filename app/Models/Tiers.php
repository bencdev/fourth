<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tiers extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    public function stand()
    {
    	return $this->belongsTo(Stands::class);
    }

    public function blocks()
    {
    	return $this->hasMany(Blocks::class);
    }

}
