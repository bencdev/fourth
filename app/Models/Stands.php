<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stands extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';

    public function blocks()
    {
    	return $this->hasMany(Tiers::class);
    }
}
